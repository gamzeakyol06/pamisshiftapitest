package Test;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import Base.Base;
import static io.restassured.RestAssured.given;

public class PUT_Shift_Test extends Base{

    String token = doPostRequestAuthorizeValidateToken(LOGIN_PAGE_URL);
    public static HashMap map = new HashMap<>();
    @BeforeTest()
    public void BeforeMethod(){

        map.put("id",63);
        map.put("name",generateRandomDataforName());
        map.put("startTime","08:00:00");
        map.put("endTime","15:59:59");
        map.put("isActive",true );
/*        map.put("isDelete",false);
        map.put("createdBy",null);
        map.put("createdDate",null);
        map.put("modifiedBy",null);
        map.put("modifiedDate",null);*/
        System.out.println(map);
    }

    @Test(priority = 1,description = "200 Success")
    public void PUT_Update_Success() throws InterruptedException, IOException {

        given().headers("Authorization","Bearer "+ token).
                contentType("application/json").
                body(map).
                when().
                put(SHIFT_PAGE_URL + "/Update?id="+63).
                then().
                statusCode(204).log().all();
    }

    @Test (priority = 2)
    public void PUT_Update_Assert_Test() throws InterruptedException, IOException {

        Response response = doGetRequest(SHIFT_PAGE_URL);
        List<Integer> jsonResponse_listid = doGetResponseListID(response);
        System.out.println(jsonResponse_listid);
        List<String > jsonResponse_listname = doGetResponseName(response);
        System.out.println(jsonResponse_listname);
        for (int i = 0; i < jsonResponse_listid.size(); i++) {
            Integer postidData = jsonResponse_listid.get(i);
                if(postidData == 63){
                    String name = jsonResponse_listname.get(i);
                    Assert.assertEquals(name, map.get("name"));
                    System.out.println(name);
                    System.out.println(map.get("name"));
                }

            }
    }
}
