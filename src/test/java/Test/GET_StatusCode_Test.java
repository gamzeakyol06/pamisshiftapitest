package Test;
import Base.Base;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

public class GET_StatusCode_Test extends Base {

    String token = doPostRequestAuthorizeValidateToken(LOGIN_PAGE_URL);
    //Shift
    @Test
    public void Test_Shift() {

        given().headers("Authorization","Bearer "+token).
                contentType("application/json").
                when().
                get(SHIFT_PAGE_URL).
                then().
                statusCode(200).log().all();
    }

    //WorkCenterShift
    @Test
    public void Test_WorkCenterShift() {

        given().headers("Authorization","Bearer "+token).
                contentType("application/json").
                when().
                get(WORKCENTERSHIFT_PAGE_URL + "").
                then().
                statusCode(200).log().all();
    }

}
