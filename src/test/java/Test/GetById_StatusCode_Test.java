package Test;
import Base.Base;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.List;
import static io.restassured.RestAssured.given;

public class GetById_StatusCode_Test extends Base{

    String token = doPostRequestAuthorizeValidateToken(LOGIN_PAGE_URL);
    @Test
    public void Test_WorkCenterShiftsByWorkCenterId() throws IOException {

        Response response = doGetRequest(WORKCENTERSHIFT_PAGE_URL);
        List<Integer> jsonResponse_listid = doGetResponseListshiftID(response);

        for (int i = 0; i < jsonResponse_listid.size(); i++) {
            String postidData = Integer.toString(jsonResponse_listid.get(i));
            System.out.println("WorkCenterShift Id " + postidData);

            given().headers("Authorization","Bearer "+token).
                    contentType(ContentType.JSON).
                    when().
                    get(WORKCENTERSHIFT_PAGE_URL+"/?Id="+postidData).
                    then().
                    statusCode(200).log().all();
        }
    }
}
