package Test;

import Base.Base;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static io.restassured.RestAssured.given;

public class DELETE_WorkCenterShift_Test extends Base {

    String token = doPostRequestAuthorizeValidateToken(LOGIN_PAGE_URL);
    public static HashMap map = new HashMap<>();
    @BeforeTest()
    public void BeforeMethod(){

    }

    @Test(description = "200 Success")
    public void Delete_Update_Success() throws InterruptedException, IOException {

        given().headers("Authorization","Bearer "+ token).
                contentType("application/json").
                when().
                delete(WORKCENTERSHIFT_PAGE_URL + "/Delete?id=127").
                then().
                statusCode(204).log().all();
    }
    @Test (priority = 2)
    public void Delete_Update_Assert_Test() throws InterruptedException, IOException {
        SoftAssert softassert = new SoftAssert();
        Response response = doGetRequest(WORKCENTERSHIFT_PAGE_URL);
        List<Integer> jsonResponse_listid = doGetResponseListID(response);
        System.out.println(jsonResponse_listid);
        List<Boolean> jsonResponse_listisDelete = doGetResponseListisDelete(response);
        System.out.println(jsonResponse_listisDelete);
        for (int i = 0; i < jsonResponse_listid.size(); i++) {
            Integer postidData = jsonResponse_listid.get(i);
            if (postidData == 127) {
                Boolean delete = jsonResponse_listisDelete.get(i);
                System.out.println(delete);
                softassert.assertEquals(delete, 1);
                System.out.println(delete);

            }
        }
    }
}
