package Test;

import Base.Base;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.HashMap;
import static io.restassured.RestAssured.given;

public class POST_WorkCenterShift_Test extends Base {

    String token = doPostRequestAuthorizeValidateToken(LOGIN_PAGE_URL);
    public static HashMap map = new HashMap<>();

    @BeforeTest()
    public void BeforeMethod(){

       //
        map.put("name",generateRandomDataforName());
        map.put("startDate","2023-03-01T16:59:25");
        map.put("endDate","2023-03-08T16:00:30");
        map.put("workCenterID", 97);
        map.put("shiftID",36);
        map.put("isDelete", false);
        map.put("isActive",true);
        map.put("createdBy", null);
        map.put("createdDate",null);
        map.put("modifiedBy",null);
        map.put("modifiedDate",null);

        System.out.println(map);
    }

    @Test (priority = 1, description = "200 Success")
    public void POST_Create_Success() throws InterruptedException, IOException {

        given().headers("Authorization","Bearer "+ token).
                contentType("application/json").
                body(map).
                when().
                post(WORKCENTERSHIFT_PAGE_URL + "/Create").
                then().
                statusCode(200).log().all();
    }

    @Test (priority = 2, description = "400 Not Success (Empty WorkCenterShift Name)")
    public void POST_Create_Not_Success() throws InterruptedException {

        map.put("name",null);
        map.put("startDate","2023-03-01T16:59:25");
        map.put("endDate","2023-03-08T16:00:30");
        map.put("workCenterID", 1);
        map.put("shiftID", 3);
        map.put("isDelete", null);
        map.put("isActive",1 );
        map.put("createdBy", null);
        map.put("createdDate",null);
        map.put("modifiedBy", null);
        System.out.println(map);

        given().headers("Authorization","Bearer "+token).
                contentType("application/json").
                body(map).
                when().
                post(WORKCENTERSHIFT_PAGE_URL + "/Create").
                then().
                statusCode(400).log().all();
    }
}
